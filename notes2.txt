{% extends 'base.html' %}

{% block content %}

  {% for message in get_flashed_messages() %}
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Holy guacamole!</strong> {{ message }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    </div>
  {% endfor%}

  <h1> Manage {{ commission_to_display.title }}'s Storyboards </h1>

  {% for storyboard in storyboards_to_display %}
    <h1> {{ storyboard.id }} : {{ storyboard.title }} : {{ storyboard.status }} </h1>
    <br/>
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-interval="5000" data-bs-ride="carousel">
      <div class="carousel-inner">

        {% for material in storyboard_materials_dict[storyboard.id] %}
          {% if loop.index == 1 %}
            <div class="carousel-item active">
              <img src="{{ url_for('static', filename='material/'+material.filename)}}" class="d-block w-100" alt="{{ material.title }}">
            </div>
          {% else %}
          <div class="carousel-item">
            <img src="{{ url_for('static', filename='material/'+material.filename)}}" class="d-block w-100" alt="{{ material.title }}">
          </div>
          {% endif %}

        {% endfor %}

      </div>
    </div>

  {% endfor %}


  <!-- Bootstrap JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


{% endblock %}
