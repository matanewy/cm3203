from flask import Flask, render_template, flash, request, redirect, url_for, send_file
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, BooleanField, SelectField, RadioField, ValidationError
from wtforms.validators import DataRequired, EqualTo, Length
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user, current_user
from flask_wtf.file import FileField
from werkzeug.utils import secure_filename
import uuid as uuid
import os
from zipfile import ZipFile

# Create Instance of flask
app = Flask(__name__)
# Init Secret Key
app.config['SECRET_KEY'] = "scarlett"
# Init Database
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.db' OLD DB DO NOT USE
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:password123@localhost/profiles'

UPLOAD_PFP = 'static/profile_picture'
app.config['UPLOAD_PFP'] = UPLOAD_PFP

UPLOAD_MATERIAL = 'static/material'
app.config['UPLOAD_MATERIAL'] = UPLOAD_MATERIAL

UPLOAD_STORYBOARD = 'static/storyboard'
app.config['UPLOAD_STORYBOARD'] = UPLOAD_STORYBOARD

db = SQLAlchemy(app)
migrate = Migrate(app, db)
# Flask_Login Schtuff
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


# DB Models
class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False, unique=True)
    bio = db.Column(db.String(200), default="No Biography")
    role = db.Column(db.String(20), nullable=False)
    status = db.Column(db.String(20), default="Active")
    profile_picture = db.Column(db.String(200))
    date_added = db.Column(db.DateTime, default=datetime.utcnow)
    password_hash = db.Column(db.String(128))
    colour_scheme = db.Column(db.String(20), default="Default")
    overall_rating = db.Column(db.String(200))
    processed_rating = db.Column(db.Float(), default=0)
    total_no_of_commission_reviews = db.Column(db.Integer(), default=0)
    average_commission_rating = db.Column(db.Float(), default=0) # List of ratings that are converted to floats -> added together to form final overall rating
    commissions = db.Column(db.String(200)) # List of user ids
    favourite_users = db.Column(db.String(200)) # List of user ids
    favourite_commissions = db.Column(db.String(200)) # List of commission ids
    no_of_favourites = db.Column(db.Integer, default=0)
    is_deleted = db.Column(db.Integer, default=0) # 0 == Not Delete, 1 == Is Deleted
    last_updated = db.Column(db.DateTime, default=datetime.utcnow)

    @property
    def password(self):
        raise AttributeError('Password is not a readable attribute!')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<Name %r>' % self.name

class Commissions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    commissioner = db.Column(db.String(200), nullable=False) # A user id
    overall_rating = db.Column(db.String(200))
    processed_rating = db.Column(db.Float(), default=0)
    medium = db.Column(db.String(25), nullable=False)
    cover_image = db.Column(db.String(200))
    genre = db.Column(db.String(25), nullable=False)
    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(200), nullable=False)
    creators = db.Column(db.String(200)) # List of user ids
    content = db.Column(db.String(2000)) # List of links to content
    storyboards = db.Column(db.String(200)) # List of storyboard ids
    status = db.Column(db.String(10), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    no_of_favourites = db.Column(db.Integer, default=0)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Title %r>' % self.title

class Messages(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sending_user = db.Column(db.Integer) # A user id
    recieving_user = db.Column(db.Integer) # A user id
    message = db.Column(db.String(200))
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

class User_Reviews(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_reviewed = db.Column(db.Integer) # A user id
    review_owner = db.Column(db.Integer) # A user id
    rating = db.Column(db.Float)
    message = db.Column(db.String(200))

class Commission_Reviews(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    commission_reviewed = db.Column(db.Integer) # A commission id
    review_owner = db.Column(db.Integer) # A user id
    rating = db.Column(db.Float)
    message = db.Column(db.String(200))

class Storyboards(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    status = db.Column(db.String(10))
    content_list = db.Column(db.String(2000))
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

class Content(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename =  db.Column(db.String(2000))
    title = db.Column(db.String(50))
    uploader = db.Column(db.Integer)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    file_type = db.Column(db.String(15))
    file_size = db.Column(db.Integer)

# Forms
class Login_Form(FlaskForm):
    email = StringField("Email Address", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Log in")

class Signup_Form(FlaskForm):
    name = StringField("Full Name", validators=[DataRequired()])
    email = StringField("Email Address", validators=[DataRequired()])
    password_hash = PasswordField('Password', validators=[DataRequired(), EqualTo('password_hash2', message='Passwords Must Match!')])
    password_hash2 = PasswordField('Confirm Password', validators=[DataRequired()])
    role = RadioField('Role')
    submit = SubmitField("Sign Up")

class Update_User_Form(FlaskForm):
    name = StringField("Full Name", validators=[DataRequired()])
    bio = StringField("Biography")
    password = PasswordField('Password', validators=[EqualTo('confirm_password', message='Passwords Must Match!')])
    confirm_password = PasswordField('Confirm Password')
    profile_picture = FileField('Profile Picture')
    role = RadioField('Role', choices=[], validators=[DataRequired()])
    submit = SubmitField("Update")

class Create_Commission_Form(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    medium = SelectField(u'Medium', choices=['Script','Movie/Film','Book/Novel','News Story','Video Game','Art'], validators=[DataRequired()])
    genre = SelectField(u'Genre', choices=['Horror','Fantasy','Romance','Dark','Gritty'], validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    status = RadioField('Status', choices=[])
    cover_image = FileField('Cover Image')
    submit = SubmitField("Create", validators=[DataRequired()])

class Update_Commission_Form(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    medium = SelectField(u'Medium', choices=['Script','Movie/Film','Book/Novel','News Story','Video Game','Art'], validators=[DataRequired()])
    genre = SelectField(u'Genre', choices=['Horror','Fantasy','Romance','Dark','Gritty'], validators=[DataRequired()])
    status = RadioField('Status', choices=[])
    description = StringField("Description", validators=[DataRequired()])
    cover_image = FileField('Cover Image')
    submit = SubmitField("Update")

class Message_Form(FlaskForm):
    message = StringField("Message", validators=[DataRequired()])
    submit = SubmitField("Send Message")

class Create_User_Review_Form(FlaskForm):
    rating = RadioField('Rating', choices=[1,2,3,4,5])
    message = StringField("Comments", validators=[DataRequired()])
    submit = SubmitField("Submit")

class Update_User_Review_Form(FlaskForm):
    rating = RadioField('Rating', choices=[1,2,3,4,5])
    message = StringField("Comments", validators=[DataRequired()])
    submit = SubmitField("Update")

class Create_Commission_Review_Form(FlaskForm):
    rating = RadioField('Rating', choices=[1,2,3,4,5])
    message = StringField("Comments", validators=[DataRequired()])
    submit = SubmitField("Submit")

class Update_Commission_Review_Form(FlaskForm):
    rating = RadioField('Rating', choices=[1,2,3,4,5])
    message = StringField("Comments", validators=[DataRequired()])
    submit = SubmitField("Update")

class Upload_Materials_Form(FlaskForm):
    file = FileField('Profile Picture', validators=[DataRequired()])
    submit = SubmitField("Upload File")

class Create_Storyboards_Form(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    status = StringField('Status', validators=[DataRequired()])
    submit = SubmitField("Create")

class Add_Frame_Form(FlaskForm):
    frame = FileField('Add Frame To Storyboard', validators=[DataRequired()])
    submit = SubmitField("Add Storyboard Frame")

class Dashboard_Form(FlaskForm):
    Select_Choices = ['Commissions' , 'Profiles']
    sort_by = SelectField(u'Sort By', choices=[], validators=[DataRequired()])
    search_type = SelectField(u'Search Type', choices=Select_Choices, validators=[DataRequired()])
    search_bar = StringField('Search')
    submit = SubmitField("Search")

class Settings_Form(FlaskForm):
    colourblind = SelectField(u'Colourblind Settings', choices=['Default','Monochrome','High Contrast'])
    status = RadioField('Account Status', choices=[])
    save_changes = SubmitField("Save Changes")

class Settings_Delete(FlaskForm):
    delete_account = StringField("Delete Account")
    delete = SubmitField("Delete")

class Confirm_Delete_Form(FlaskForm):
    confirm = SubmitField('Confirm')


# Routes
@app.route('/', methods=['GET', 'POST'])
def index():
    return redirect(url_for('signup'))

@app.route('/logout', methods=['GET','POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    login_form = Login_Form()
    signup_form = Signup_Form()
    choices = ['Writer','Audience']
    signup_form.role.choices = choices

    user_list = Users.query.order_by(Users.date_added)
    #Validate Form
    if signup_form.validate_on_submit():
        user = Users.query.filter_by(email=signup_form.email.data).first()
        if user is None:
            # Hash password
            hashed_pw = generate_password_hash(signup_form.password_hash.data, "sha256")
            user = Users(name=signup_form.name.data, email=signup_form.email.data, password_hash=hashed_pw, role=signup_form.role.data)
            db.session.add(user)
            db.session.commit()
            # Reset fields
            signup_form.name.data = ''
            signup_form.email.data = ''
            signup_form.password_hash.data = ''
            signup_form.password_hash2.data = ''
            flash("Success! Account Created")
            return redirect(url_for('login'))
        else:
            return render_template("login.html", login_form=login_form)

    return render_template("signup.html", signup_form=signup_form, user_list=user_list, choices=choices)

@app.route('/login', methods=['GET', 'POST'])
def login():
    #Variables
    email = None
    password = None
    pw_to_check = None
    passed = None
    login_form = Login_Form()
    # Validate form
    if login_form.validate_on_submit():
        email = login_form.email.data
        password = login_form.password.data
        user_to_check = Users.query.filter_by(email=email).first()
        # Check if a user exists
        if user_to_check != None:
            # Check if has been deleted
            if user_to_check.is_deleted == 0:
                # Check hashed password
                passed = check_password_hash(user_to_check.password_hash, password)
                # Update current_user
                if passed == True:
                    login_user(user_to_check)
                    flash("Logged in!")
                    return redirect(url_for('dashboard'))
                else:
                    # Reset fields
                    login_form.email.data = ''
                    login_form.password.data = ''
                    flash("Email and Password do not match!")
                    return render_template("login.html", login_form=login_form, email=email, password=password)
            else:
                flash("This User Has Been Deleted, Please Create a New Account")
                return render_template("login.html", login_form=login_form, email=email, password=password)
        else:
            flash("Email and Password do not match!")
            return render_template("login.html", current_user=current_user, login_form=login_form, email=email, password=password)



    else:
        return render_template("login.html", current_user=current_user, login_form=login_form, email=email, password=password)

@app.route('/update_user/<int:id>', methods=['GET','POST'])
def update_user(id):
    update_user_form = Update_User_Form()
    choices = ['Writer','Audience']
    update_user_form.role.choices = choices
    user_to_update = Users.query.get_or_404(id)
    update_user_form.role.default = user_to_update.role
    update_user_form.process()
    if request.method == "POST":
        # Update User Table Columns #
        user_to_update.name = request.form['name']
        if request.form['password'] != "":
            if request.form['password'] == request.form['confirm_password']:
                user_to_update.password_hash = generate_password_hash(request.form['password'], "sha256")
        user_to_update.bio = request.form['bio']
        user_to_update.role = request.form['role']
        user_to_update.profile_picture = request.files['profile_picture']
        if user_to_update.profile_picture != None:
            # Give PFP Unique Filename #
            secure = secure_filename(user_to_update.profile_picture.filename)
            secure = str(uuid.uuid1()) + "_" + secure
            # Save Image #
            user_to_update.profile_picture.save(os.path.join(app.config['UPLOAD_PFP'], secure))
            user_to_update.profile_picture = secure
        user_to_update.last_updated = datetime.utcnow()
        # Commit to DB #
        try:
            db.session.commit()
            flash("User updated successfully!")
            return redirect(url_for('view_profile', id=id))
        except:
            flash("Error :/")
            return redirect(url_for('view_profile', id=id))
    else:
        return render_template("update_user.html", update_user_form=update_user_form, user_to_update=user_to_update,
        choices=choices)

@app.route('/remove_profile_picture/<int:id>', methods=['GET','POST'])
def remove_profile_picture(id):
    user_to_update = Users.query.filter_by(id=id).first()
    user_to_update.profile_picture = ""
    db.session.commit()
    return redirect(url_for('view_profile', id=id))

@app.route('/delete_user/<int:id>', methods=['GET','POST'])
def delete_user(id):
    user_to_delete = Users.query.get_or_404(id)
    user_to_delete.is_deleted = 1
    try:
        db.session.commit()
        flash("User deleted successfully!")
        return redirect(url_for('logout'))
    except:
        flash("Problem deleting user!")
    return redirect(url_for('signup'))

@app.route('/create_commission', methods=['GET', 'POST'])
def create_commission():
    create_commission_form = Create_Commission_Form()
    commission_list = Commissions.query.order_by(Commissions.date_created)
    choices =['Starting','Active','Finished']
    create_commission_form.status.choices = choices
    create_commission_form.status.default = "Starting"
    create_commission_form.process()
    #Validate Form
    if request.method=="POST":
        # DB Data
        title = request.form['title']
        medium = request.form['medium']
        genre = request.form['genre']
        status = request.form['status']
        description = request.form['description']
        commissioner = current_user.id

        cover_image = request.files['cover_image']
        if cover_image.filename != None and cover_image.filename != "":
            # Give PFP Unique Filename #
            secure = secure_filename(cover_image.filename)
            secure = str(uuid.uuid1()) + "_" + secure
            # Save Image #
            cover_image.save(os.path.join(app.config['UPLOAD_MATERIAL'], secure))
            cover_image = secure
        else:
            secure = ""

        flash("Commission created successfully")
        commission = Commissions(title=title, medium=medium, genre=genre, status=status, description=description,
        commissioner=commissioner, cover_image=secure)
        db.session.add(commission)
        db.session.commit()

        new_commission_id = commission.id
        if current_user.commissions == None:
            current_user.commissions = ";" + str(new_commission_id)
        else:
            current_user.commissions = current_user.commissions + ";" + str(new_commission_id)
        db.session.commit()
        return redirect(url_for("dashboard"))

    return render_template("create_commission.html", current_page='create_commission',
    create_commission_form=create_commission_form, commission_list=commission_list, choices=choices)

@app.route('/update_commission/<int:id>', methods=['GET','POST'])
def update_commission(id):
    update_commission_form = Update_Commission_Form()
    choices = ['Starting','Active','Finished']
    update_commission_form.status.choices = choices
    commission_to_update = Commissions.query.get_or_404(id)

    if request.method == "POST":
        commission_to_update.title = request.form['title']
        commission_to_update.medium = request.form['medium']
        commission_to_update.genre = request.form['genre']
        commission_to_update.status = request.form['status']
        commission_to_update.description = request.form['description']
        commission_to_update.cover_image = request.files['cover_image']
        if commission_to_update.cover_image != None:
            # Give PFP Unique Filename #
            secure = secure_filename(commission_to_update.cover_image.filename)
            secure = str(uuid.uuid1()) + "_" + secure
            # Save Image #
            commission_to_update.cover_image.save(os.path.join(app.config['UPLOAD_MATERIAL'], secure))
            commission_to_update.cover_image = secure
        commission_to_update.last_updated = datetime.utcnow()
        try:
            db.session.commit()
            flash("Commission updated successfully!")
        except:
            flash("Error :/")

        return redirect(url_for('view_commission', id=id))

    update_commission_form.status.default = commission_to_update.status
    update_commission_form.medium.default = commission_to_update.medium
    update_commission_form.genre.default = commission_to_update.genre
    update_commission_form.process()

    return render_template("update_commission.html", update_commission_form=update_commission_form,
    commission_to_update=commission_to_update, choices=choices)

@app.route('/view_commission/<int:id>', methods=['GET','POST'])
def view_commission(id):
    overall_rating = 0
    no_of_reviews = 0
    creators = None
    current_user_review_id = None
    favourited = False
    reviewed = False
    is_creator = False
    is_commissioner = False
    commission_to_display = Commissions.query.get_or_404(id)
    commission_reviews = Commission_Reviews.query.filter_by(commission_reviewed=id)
    commissioner = Users.query.filter_by(id=commission_to_display.commissioner).first()

    # Check if commission is reviewed by current user
    if current_user.is_authenticated:
        if commission_reviews != None:
            for review in commission_reviews:
                overall_rating = overall_rating + int(review.rating)
                no_of_reviews = no_of_reviews + 1
                if review.review_owner == current_user.id:
                    current_user_review_id = review.id
                    reviewed = True
            if overall_rating > 0:
                overall_rating = overall_rating / no_of_reviews

        # Check if current_user is a crator for the commission
        creators = commission_to_display.creators
        if creators != None:
            creators = creators.split(';')
            for creator in creators:
                if creator == str(current_user.id):
                    is_creator = True

        # Check if current user is commissioner #
        if int(current_user.id) == int(commission_to_display.commissioner):
            is_commissioner = True
        else:
            is_commissioner = False
        # Check if commission is favourited by current user
        if current_user.favourite_commissions != None:
            favourited_commissions = current_user.favourite_commissions.split(";")
            for id in favourited_commissions:
                if str(id) == str(commission_to_display.id):
                    favourited = True

        # Materials For Download Button
        materials_to_download = []
        material_ids = commission_to_display.content
        if material_ids != None:
            material_ids = material_ids.split(';')
            for id in material_ids:
                if id != "":
                    # Get File #
                    material = Content.query.filter_by(id=id).first()
                    materials_to_download.append(material)

    # Retrieve Commission_Reviews For Carousel #
    review_list = Commission_Reviews.query.filter_by(commission_reviewed=commission_to_display.id).all()
    review_owner_list = []
    if review_list != []:
        for review in review_list:
            review_owner_list.append( Users.query.filter_by(id=review.review_owner).first() )

    return render_template("view_commission.html", commission_to_display=commission_to_display, favourited=favourited,
    reviewed=reviewed, current_user_review_id=current_user_review_id, overall_rating=overall_rating, is_creator=is_creator,
    commissioner=commissioner, review_list=review_list, review_owner_list=review_owner_list, is_commissioner=is_commissioner,
    materials_to_download=materials_to_download)

@app.route('/download_commission_most_recent_materials/<int:id>', methods=['GET','POST'])
def download_commission_most_recent_materials(id):
    commission_to_display = Commissions.query.get_or_404(id)
    material_ids = commission_to_display.content
    zipf = None

    if material_ids != None:
        material_ids = material_ids.split(';')
        flash("Latest Version Downloaded")
        # Create Zip File #
        zipf = ZipFile('Download.zip', 'w')
        for mid in material_ids:
            if mid != "":
                # Get File #
                material_to_download = Content.query.filter_by(id=mid).first()
                path = "./" + app.config['UPLOAD_MATERIAL'] + "/" + material_to_download.filename
                zipf.write(path)
        zipf.close()

        return send_file('Download.zip', mimetype='zip', attachment_filename='Download.zip', as_attachment=True)
    else:
        flash("No Materials to Download")
        return redirect(url_for("view_commission", id=id))

@app.route('/delete_commission/<int:id>', methods=['GET','POST'])
def delete_commission(id):
    commission_to_delete = Commissions.query.get_or_404(id)
    # Reload Create_Commission Page Variables
    title = None
    medium = None
    genre = None
    status = None
    description = None
    create_commission_form = Create_Commission_Form()
    commission_list = Commissions.query.order_by(Commissions.date_created)
    try:
        db.session.delete(commission_to_delete)
        db.session.commit()
        flash("Commission deleted successfully!")
    except:
        flash("Problem deleting commission!")
    return render_template("create_commission.html", create_commission_form=create_commission_form, title=title, medium=medium, genre=genre, status=status, description=description, commission_list=commission_list)

@app.route('/dashboard', methods=['GET','POST'])

def dashboard():
    dashboard_form = Dashboard_Form()
    favourited_commissions = []
    if current_user.is_authenticated:
        if current_user.favourite_commissions != None:
            favourited_commissions = current_user.favourite_commissions.split(";")

    Sort_Choices = ['Highest Rating', 'Lowest Rating', 'Newest', 'Oldest', 'Last Updated']
    dashboard_form.sort_by.choices = Sort_Choices
    sort_by = None
    search_type = "Commissions" # Set Default

    if request.method=='POST':

        search_text = dashboard_form.search_bar.data
        sort_by = dashboard_form.sort_by.data

        if dashboard_form.search_type.data == 'Commissions':
            search_type='Commissions'
            # Array of Column Names for Display #
            table_column_name = ['commissioner','title','medium','genre','description','status','overall_rating','processed_rating'
            'creators','content','storyboards']
            # List of Profiles Relevant to Search #
            list_to_display = Commissions.query.order_by(Commissions.date_created.desc())
            if sort_by == "" or sort_by == "Highest Rating":
                list_to_display = Commissions.query.filter(Commissions.description.like('%' + search_text + '%')).order_by(Commissions.processed_rating.desc())
            elif sort_by == "Lowest Rating":
                list_to_display = Commissions.query.filter(Commissions.description.like('%' + search_text + '%')).order_by(Commissions.processed_rating.asc())
            elif sort_by == "Newest":
                list_to_display = Commissions.query.filter(Commissions.description.like('%' + search_text + '%')).order_by(Commissions.date_created.asc())
            elif sort_by == "Last Updated":
                list_to_display = Commissions.query.filter(Commissions.description.like('%' + search_text + '%')).order_by(Commissions.last_updated.desc())
            else:
                list_to_display = Commissions.query.filter(Commissions.description.like('%' + search_text + '%')).order_by(Commissions.date_created.desc())

        elif dashboard_form.search_type.data == 'Profiles':
            search_type='Profiles'
            # Array of Column Names for Display #
            table_column_name = ['name','email','bio','role','status','profile_picture','password_hash','overall_rating','processed_rating'
            'commissions','favourite_users','favourite_commissions']
            # List of Profiles Relevant to Search #
            if sort_by == "" or sort_by == "Highest Rating":
                list_to_display = Users.query.filter(Users.name.like('%' + search_text + '%')).order_by(Users.processed_rating.desc())
            elif sort_by == "Lowest Rating":
                list_to_display = Users.query.filter(Users.bio.like('%' + search_text + '%')).order_by(Users.processed_rating.asc())
            elif sort_by == "Newest":
                list_to_display = Users.query.filter(Users.bio.like('%' + search_text + '%')).order_by(Users.date_added.asc())
            elif sort_by == "Last Updated":
                list_to_display = Users.query.filter(Users.bio.like('%' + search_text + '%')).order_by(Users.last_updated.desc())
            else:
                list_to_display = Users.query.filter(Users.bio.like('%' + search_text + '%')).order_by(Users.date_added.desc())

    else:
        # Array of Column Names for Display #
        table_column_name = ['commissioner','title','medium','genre','description','status','overall_rating','processed_rating'
        'creators','content','storyboards']
        # List of Commissions Sorted by Current Sort Field Value #
        search_type = 'Commissions'
        # Get Current Sort-By and Sort List_to_Display #
        if sort_by == "" or sort_by == "Highest Rating":
            list_to_display = Commissions.query.order_by(Commissions.processed_rating.desc())
        elif sort_by == "Lowest Rating":
            list_to_display = Commissions.query.order_by(Commissions.processed_rating.asc())
        elif sort_by == "Newest":
            list_to_display = Commissions.query.order_by(Commissions.date_created.asc())
        elif sort_by == "Last Updated":
            list_to_display = Commissions.query.order_by(Commissions.last_updated.desc())
        else:
            list_to_display = Commissions.query.order_by(Commissions.date_created.desc())

    return render_template('dashboard.html', current_page="dashboard", list_to_display=list_to_display, dashboard_form=dashboard_form,
    search_type=search_type, table_column_name=table_column_name, favourited_commissions=favourited_commissions)

@app.route('/message/<int:id>', methods=['GET','POST'])
@login_required
def message(id):
    other_user = Users.query.get_or_404(id)
    message = None
    message_form = Message_Form()
    temp_list = Messages.query.filter_by(sending_user=current_user.id).order_by(Messages.date_created.asc()).all()
    sent_message_list = []
    for message in temp_list:
        sent_message_list.append(message)
    temp_list = Messages.query.filter_by(recieving_user=current_user.id).order_by(Messages.date_created.asc()).all()
    recieved_message_list = []
    for message in temp_list:
        recieved_message_list.append(message)
    # Combine two sorted lists #
    len1 = len(sent_message_list)
    len2 = len(recieved_message_list)
    full_message_list = []
    i = 0
    j = 0
    while i < len1 and j < len2:
        if sent_message_list[i].date_created < recieved_message_list[j].date_created:
            full_message_list.append(sent_message_list[i])
            i = i + 1
        else:
            full_message_list.append(recieved_message_list[j])
            j = j + 1
    full_message_list = full_message_list + sent_message_list[i:] + recieved_message_list[j:]

    if message_form.validate_on_submit():
        message = Messages(message=message_form.message.data, recieving_user=id, sending_user=current_user.id)
        db.session.add(message)
        db.session.commit()
        # Reset fields
        message_form.message.data = ''
        message = ''
        return redirect(url_for('message', id=id))

    return render_template('message.html', message_form=message_form, message=message, other_user=other_user,
    sent_message_list=sent_message_list, recieved_message_list=recieved_message_list, full_message_list=full_message_list)

@app.route('/delete_message/<int:id>', methods=['GET','POST'])
def delete_message(id):
    message_to_delete = Messages.query.get_or_404(id)
    # Message Varaibles
    other_user = Users.query.get_or_404(message_to_delete.recieving_user)
    message = None
    message_form = Message_Form()
    sent_message_list = Messages.query.filter_by(sending_user=current_user.id)
    recieved_message_list = Messages.query.filter_by(recieving_user=current_user.id)
    try:
        db.session.delete(message_to_delete)
        db.session.commit()
        flash("Message Unsent!")
    except:
        flash("Problem Unsending Message!")
    return redirect(url_for('message', id=other_user.id))

@app.route('/view_profile/<int:id>', methods=['GET','POST'])
def view_profile(id):
    no_of_commissions = 0
    no_of_user_reviews = 0
    overall_rating = 0
    no_of_reviews = 0
    current_user_review_id = None
    favourited = False
    reviewed = False
    user_to_display = Users.query.get_or_404(id)
    user_reviews = User_Reviews.query.filter_by(user_reviewed=id)

    # Check if user is reviewed by current user
    if current_user.is_authenticated:
        if current_user.id == user_to_display.id:
            current_page = "current_profile"
        else:
            current_page = ""

        if user_reviews != None:
            for review in user_reviews:
                overall_rating = overall_rating + int(review.rating)
                no_of_reviews = no_of_reviews + 1
                if review.review_owner == current_user.id:
                    current_user_review_id = review.id
                    reviewed = True
            if overall_rating > 0:
                overall_rating = overall_rating / no_of_reviews

        # Check if user is favourited by current user
        if current_user.favourite_users == None:
            favourited = False
        else:
            favourited_users = current_user.favourite_users.split(";")
            for id in favourited_users:
                if str(id) == str(user_to_display.id):
                    favourited = True

    # Save Number of User Reviews to Variable
    no_of_user_reviews = User_Reviews.query.filter_by(user_reviewed=id).count()
    # Save Number of Created Commissions to Variable
    if user_to_display.commissions != None:
        no_of_commissions = len(user_to_display.commissions.split(';')) -1 # Account for phantom '0' before first ';'
    # For User Commission Carousel #
    user_commission_list = []
    if user_to_display.commissions != None:
        user_commission_ids = user_to_display.commissions.split(';')
        for id in user_commission_ids:
            user_commission_list.append(Commissions.query.filter_by(id=id).first())
    # For Favourite Commission Carousel #
    favourite_commission_list = []
    if user_to_display.favourite_commissions != None:
        favourite_user_ids = user_to_display.favourite_commissions.split(';')
        for id in favourite_user_ids:
            favourite_commission_list.append(Commissions.query.filter_by(id=id).first())
    # Retrieve User_Reviews For Carousel #
    review_list = User_Reviews.query.filter_by(user_reviewed=user_to_display.id).all()
    review_owner_list = []
    if review_list != []:
        for review in review_list:
            review_owner_list.append( Users.query.filter_by(id=review.review_owner).first() )


    return render_template("view_profile.html", current_page=current_page, user_to_display=user_to_display, favourited=favourited,
    reviewed=reviewed, current_user_review_id=current_user_review_id, overall_rating=overall_rating,
    no_of_user_reviews=no_of_user_reviews, no_of_commissions=no_of_commissions, user_commission_list=user_commission_list,
    favourite_commission_list=favourite_commission_list, review_list=review_list, review_owner_list=review_owner_list)

@app.route('/create_user_review/<int:id>', methods=['GET','POST'])
def create_user_review(id):
    rating = ''
    message = ''
    user_to_display = Users.query.get_or_404(id)
    create_user_review_form = Create_User_Review_Form()
    review_list = User_Reviews.query.filter_by(user_reviewed=id)
    #Validate Form
    if create_user_review_form.validate_on_submit():
        # User_Review DB Data
        rating = create_user_review_form.rating.data
        if rating == "1":
            rating = 5
        elif rating == "2":
            rating = 4
        elif rating == "4":
            rating = 2
        elif rating == "5":
            rating = 1
        else:
            rating = 3

        message = create_user_review_form.message.data
        review_owner = current_user.id
        user_reviewed = id
        # Update User Ratings Info
        user_to_update = user_to_display
        if user_to_update.overall_rating == None:
            user_to_update.overall_rating = ""
        user_to_update.overall_rating = user_to_update.overall_rating + ";" + str(rating)

        split_ratings = user_to_update.overall_rating.split(';')

        user_review = User_Reviews(rating=rating, message=message, review_owner=review_owner, user_reviewed=user_reviewed)
        db.session.add(user_review)
        db.session.commit()
        flash("Review Submitted Successfully")

        calculated = 0
        no_of_reviews = 0
        user_reviews = User_Reviews.query.filter_by(user_reviewed=id)
        for review in user_reviews:
            calculated = calculated + int(review.rating)
            no_of_reviews = no_of_reviews + 1
        if calculated > 0:
            calculated = calculated / no_of_reviews
        user_to_update.processed_rating = calculated
        # Add to DB
        db.session.commit()

        # Reset fields
        create_user_review_form.rating.data = ''
        create_user_review_form.message.data = ''
        return redirect(url_for('view_profile', id=user_reviewed))

    return render_template("create_user_review.html", create_user_review_form=create_user_review_form, user_to_display=user_to_display, review_list=review_list,
    rating=rating, message=message)

@app.route('/update_user_review/<int:id>', methods=['GET','POST'])
def update_user_review(id):
    update_user_review_form = Update_User_Review_Form()
    review_to_update = User_Reviews.query.get_or_404(id)
    old_rating = str(review_to_update.rating)
    reviewed_user = Users.query.get_or_404(review_to_update.user_reviewed)
    if request.method == "POST":
        # Review changes to push to DB
        if request.form['rating'] == 5:
            review_to_update.rating = "1"
        elif request.form['rating'] == "4":
            review_to_update.rating = "2"
        elif request.form['rating'] == "2":
            review_to_update.rating = "4"
        elif request.form['rating'] == "1":
            review_to_update.rating = "5"
        else:
            review_to_update.rating = "3"

        review_to_update.message = request.form['message']
        # User rating changes to push to DB
        user_to_update = reviewed_user
        user_to_update.overall_rating = user_to_update.overall_rating.replace(old_rating.strip(".0"), review_to_update.rating.strip(".0"), 1)
        # Commit changes
        db.session.commit()

        calculated = 0
        no_of_reviews = 0

        user_reviews = User_Reviews.query.filter_by(user_reviewed=user_to_update.id)
        for review in user_reviews:
            calculated = calculated + int(review.rating)
            no_of_reviews = no_of_reviews + 1
        if calculated > 0:
            calculated = calculated / no_of_reviews
        user_to_update.processed_rating = calculated
        db.session.commit()

        flash("Review updated successfully!")
        return render_template("update_user_review.html", update_user_review_form=update_user_review_form, review_to_update=review_to_update, reviewed_user=reviewed_user)
    else:
        return render_template("update_user_review.html",
        update_user_review_form=update_user_review_form,
        review_to_update=review_to_update, reviewed_user=reviewed_user)

@app.route('/delete_user_review/<int:id>', methods=['GET','POST'])
def delete_user_review(id):
    # Load Variables
    review_to_delete = User_Reviews.query.get_or_404(id)
    user_to_display = Users.query.get_or_404(review_to_delete.user_reviewed)
    # Remove from Users.Overall_Rating
    old_rating = ";" + str(review_to_delete.rating)
    user_to_display.overall_rating = user_to_display.overall_rating.replace(old_rating.strip(".0"), "", 1)
    db.session.delete(review_to_delete)
    db.session.commit()

    calculated = 0
    no_of_reviews = 0
    user_reviews = User_Reviews.query.filter_by(user_reviewed=user_to_display.id)
    for review in user_reviews:
        calculated = calculated + int(review.rating)
        no_of_reviews = no_of_reviews + 1
    if calculated > 0:
        calculated = calculated / no_of_reviews
    user_to_display.processed_rating = calculated
    db.session.commit()

    flash("Review deleted successfully!")

    return redirect(url_for('view_profile', id=user_to_display.id))

#@app.route('/view_user_reviews/<int:id>', methods=['GET','POST'])
#def view_user_reviews(id):
#    # Load Variables
#    review_list = User_Reviews.query.filter_by(user_reviewed=id)
#    reviewed_user = Users.query.filter_by(id=id).first()
#    return render_template('view_user_reviews.html', review_list=review_list, reviewed_user=reviewed_user)

@app.route('/create_commission_review/<int:id>', methods=['GET','POST'])
def create_commission_review(id):
    rating = ''
    message = ''
    commission_to_display = Commissions.query.get_or_404(id)
    create_commission_review_form = Create_Commission_Review_Form()
    review_list = Commission_Reviews.query.filter_by(commission_reviewed=id)

    #Validate Form
    if create_commission_review_form.validate_on_submit():
        # Fix Rating
        if create_commission_review_form.rating.data == "1":
            rating = "5"
        elif create_commission_review_form.rating.data == "2":
            rating = "4"
        elif create_commission_review_form.rating.data == "4":
            rating = "2"
        elif create_commission_review_form.rating.data == "5":
            rating = "1"
        else:
            rating = "3"
        message = create_commission_review_form.message.data
        review_owner = current_user.id
        commission_reviewed = id
        # Update Commission Ratings Info
        commission_to_update = commission_to_display
        if commission_to_update.overall_rating == None:
            commission_to_update.overall_rating = ""
        commission_to_update.overall_rating = commission_to_update.overall_rating + ";" + str(rating)

        # Update Commissioner's Average Commission Review #
        commission_creator = Users.query.filter_by(id=commission_to_update.commissioner).first()
        commission_creator.total_no_of_commission_reviews = commission_creator.total_no_of_commission_reviews + 1
        commission_creator.average_commission_rating = commission_creator.average_commission_rating + float(rating)
        # Add to DB
        db.session.commit()

        calculated = 0
        no_of_reviews = 0
        comm_reviews = Commissions.query.filter_by(id=id).first()
        comm_reviews = comm_reviews.overall_rating.split(";")
        for review in comm_reviews:
            if review != "":
                calculated = calculated + float(review)
                no_of_reviews = no_of_reviews + 1
        if calculated > 0 and no_of_reviews > 0:
            calculated = calculated / no_of_reviews
        commission_to_update.processed_rating = calculated
        # Add to DB
        db.session.commit()

        commission_review = Commission_Reviews(rating=rating, message=message, review_owner=review_owner, commission_reviewed=commission_reviewed)
        db.session.add(commission_review)
        db.session.commit()
        # Reset fields
        create_commission_review_form.rating.data = ''
        create_commission_review_form.message.data = ''
        return redirect(url_for('view_commission', id=commission_reviewed))

    return render_template("create_commission_review.html", create_commission_review_form=create_commission_review_form, commission_to_display=commission_to_display, review_list=review_list,
    rating=rating, message=message)

@app.route('/update_commission_review/<int:id>', methods=['GET','POST'])
def update_commission_review(id):
    update_commission_review_form = Update_Commission_Review_Form()
    review_to_update = Commission_Reviews.query.get_or_404(id)
    old_rating = str(review_to_update.rating)
    reviewed_commission = Commissions.query.get_or_404(review_to_update.commission_reviewed)

    if update_commission_review_form.validate_on_submit():
        if request.method == "POST":


            # Fix Rating
            if update_commission_review_form.rating.data == "1":
                rating = "5"
            elif update_commission_review_form.rating.data == "2":
                rating = "4"
            elif update_commission_review_form.rating.data == "4":
                rating = "2"
            elif update_commission_review_form.rating.data == "5":
                rating = "1"
            elif update_commission_review_form.rating.data == "3":
                rating = "3"
            else:
                rating = "NONE"

            # Update Commissioner's Average Commission Review #
            commission_creator = Users.query.filter_by(id=reviewed_commission.commissioner).first()
            difference = review_to_update.rating - float(rating)
            commission_creator.average_commission_rating = commission_creator.average_commission_rating - difference
            # Review changes to push to DB
            review_to_update.rating = rating
            review_to_update.message = request.form['message']
            # Commission rating changes to push to DB
            commission_to_update = reviewed_commission
            commission_to_update.overall_rating = commission_to_update.overall_rating.replace(old_rating.strip(".0"), review_to_update.rating.strip(".0"), 1)

            calculated = 0
            no_of_reviews = 0
            comm_reviews = Commissions.query.filter_by(id=commission_to_update.id).first()
            comm_reviews = comm_reviews.overall_rating.split(";")
            for review in comm_reviews:
                if review != "":
                    calculated = calculated + float(review)
                    no_of_reviews = no_of_reviews + 1
            if calculated > 0 and no_of_reviews > 0:
                calculated = calculated / no_of_reviews
            commission_to_update.processed_rating = calculated

            # Commit changes
            db.session.commit()

            flash("Review updated successfully!")
            return redirect(url_for("view_commission", id=reviewed_commission.id))

    return render_template("update_commission_review.html",
    update_commission_review_form=update_commission_review_form,
    review_to_update=review_to_update, reviewed_commission=reviewed_commission)

@app.route('/delete_commission_review/<int:id>', methods=['GET','POST'])
def delete_commission_review(id):
    # Load Variables
    review_to_delete = Commission_Reviews.query.get_or_404(id)
    commission_to_display = Commissions.query.get_or_404(review_to_delete.commission_reviewed)
    # Remove from Commission.Overall_Rating
    old_rating = ";" + str(review_to_delete.rating)
    commission_to_display.overall_rating = commission_to_display.overall_rating.replace(old_rating.strip(".0"), "", 1)
    # Update Commissioner's Average Commission Review #
    commission_creator = Users.query.filter_by(id=commission_to_display.commissioner).first()
    commission_creator.total_no_of_commission_reviews = commission_creator.total_no_of_commission_reviews - 1
    commission_creator.average_commission_rating = commission_creator.average_commission_rating - review_to_delete.rating

    calculated = 0
    no_of_reviews = 0
    comm_reviews = Commissions.query.filter_by(id=commission_to_display.id).first()
    comm_reviews = comm_reviews.overall_rating.split(";")
    for review in comm_reviews:
        if review != "":
            calculated = calculated + float(review)
            no_of_reviews = no_of_reviews + 1
    if calculated > 0 and no_of_reviews > 0:
        calculated = calculated / no_of_reviews
    commission_to_display.processed_rating = calculated

    db.session.delete(review_to_delete)
    db.session.commit()

    flash("Review deleted successfully!")

    return redirect(url_for('view_commission', id=commission_to_display.id))

#@app.route('/view_commission_reviews/<int:id>', methods=['GET','POST'])
#def view_commission_reviews(id):
#    # Load Variables
#    review_list = Commission_Reviews.query.filter_by(commission_reviewed=id)
#    reviewed_commission = Commissions.query.filter_by(id=id).first()
#    return render_template('view_commission_reviews.html', review_list=review_list, reviewed_commission=reviewed_commission)

@app.route('/favourite_user/<int:id>', methods=['GET','POST'])
def favourite_user(id):
    # Load Variables
    user_to_favourite = Users.query.filter_by(id=id).first()
    user_to_update = Users.query.filter_by(id=current_user.id).first()
    if user_to_update.favourite_users == None:
        user_to_update.favourite_users = ";" + str(user_to_favourite.id)
    else:
        user_to_update.favourite_users = user_to_update.favourite_users + ";" + str(user_to_favourite.id)

    user_to_favourite.no_of_favourites = user_to_favourite.no_of_favourites + 1
    db.session.commit()
    return redirect(url_for('view_profile', id=id))

@app.route('/unfavourite_user/<int:id>', methods=['GET','POST'])
def unfavourite_user(id):
    # Load Variables
    user_to_unfavourite = Users.query.filter_by(id=id).first()
    user_to_update = Users.query.filter_by(id=current_user.id).first()
    str_to_remove = ";" + str(id)
    user_to_update.favourite_users = user_to_update.favourite_users.replace(str_to_remove, "", 1)

    user_to_unfavourite.no_of_favourites = user_to_unfavourite.no_of_favourites - 1
    db.session.commit()
    return redirect(url_for('view_profile', id=id))

@app.route('/favourite_commission/<int:id>', methods=['GET','POST'])
def favourite_commission(id):
    # Load Variables
    commission_to_favourite = Commissions.query.filter_by(id=id).first()
    user_to_update = Users.query.filter_by(id=current_user.id).first()
    if user_to_update.favourite_commissions == None:
        user_to_update.favourite_commissions = ";" + str(id)
    else:
        user_to_update.favourite_commissions = user_to_update.favourite_commissions + ";" + str(id)

    commission_to_favourite.no_of_favourites = commission_to_favourite.no_of_favourites + 1
    db.session.commit()
    return redirect(url_for('view_commission', id=id))

@app.route('/unfavourite_commission/<int:id>', methods=['GET','POST'])
def unfavourite_commission(id):
    # Load Variables
    commission_to_unfavourite = Commissions.query.filter_by(id=id).first()
    user_to_update = Users.query.filter_by(id=current_user.id).first()
    str_to_remove = ";" + str(id)
    user_to_update.favourite_commissions = user_to_update.favourite_commissions.replace(str_to_remove, "", 1)

    commission_to_unfavourite.no_of_favourites = commission_to_unfavourite.no_of_favourites - 1
    db.session.commit()
    return redirect(url_for('view_commission', id=id))

@app.route('/add_creator/<int:id>', methods=['GET','POST'])
def add_creator(id):
    # Load Variables
    commission_to_update = Commissions.query.filter_by(id=id).first()
    if commission_to_update.creators == None:
        commission_to_update.creators = ";" + str(current_user.id)
    else:
        commission_to_update.creators = commission_to_update.creators + ";" + str(current_user.id)
    db.session.commit()
    return redirect(url_for('view_commission', id=id))

@app.route('/remove_creator/<int:id>', methods=['GET','POST'])
def remove_creator(id):
    # Load Variables
    commission_to_update = Commissions.query.filter_by(id=id).first()
    str_to_remove = ";" + str(current_user.id)
    commission_to_update.creators = commission_to_update.creators.replace(str_to_remove, "", 1)
    db.session.commit()
    return redirect(url_for('view_commission', id=id))

@app.route('/manage_materials/<int:id>', methods=['GET','POST'])
def manage_materials(id):
    #Load Variables
    commission_to_display = Commissions.query.filter_by(id=id).first()
    upload_materials_form = Upload_Materials_Form()
    materials_to_display = []
    uploaders_to_display = []
    storyboards_to_display = []
    storyboard_materials_dict = {}
    add_frame_form = Add_Frame_Form()
    create_storyboards_form = Create_Storyboards_Form()

    material_ids = commission_to_display.content
    if material_ids != None:
        material_ids = material_ids.split(';')

        for id in material_ids:
            if id != "":
                # Get File #
                material = Content.query.filter_by(id=id).first()
                materials_to_display.append(material)
                # Get Uploader Names #
                uploader = Users.query.filter_by(id=material.uploader).first()
                if uploader.is_deleted == 0:
                    uploaders_to_display.append(uploader.name)
                else:
                    uploaders_to_display.append("Deleted User")
    else:
        materials_to_display = []

    storyboard_ids = commission_to_display.storyboards
    if storyboard_ids != None:
        storyboard_ids = storyboard_ids.split(';')
        for id in storyboard_ids:
            if id != "":
                storyboard = Storyboards.query.filter_by(id=id).first()
                # List of materials
                if storyboard.content_list != None:
                    MidList = storyboard.content_list.split(';')
                    storyboard_materials_to_display = []
                    for Mid in MidList:
                        if Mid != "":
                            material = Content.query.filter_by(id=Mid).first()
                            storyboard_materials_to_display.append(material)
                    storyboard_materials_dict.update({storyboard.id: storyboard_materials_to_display})
                # List of storyboards
                storyboards_to_display.append(storyboard)
    else:
        storyboards_to_display = []

    return render_template('manage_materials.html', commission_to_display=commission_to_display, upload_materials_form=upload_materials_form,
    materials_to_display=materials_to_display, uploaders_to_display=uploaders_to_display, create_storyboards_form=create_storyboards_form, storyboards_to_display=storyboards_to_display,
    add_frame_form=add_frame_form, storyboard_materials_dict=storyboard_materials_dict)

@app.route('/add_material/<int:Cid>', methods=['GET','POST'])
def add_material(Cid):
  commission_to_display = Commissions.query.filter_by(id=Cid).first()
  upload_file = request.files['file']
  # Give PFP Unique Filename
  title = upload_file.filename
  secure = secure_filename(title)
  secure = str(uuid.uuid1()) + "_" + secure
  file_type = title.split('.')[1]
  # Save Image
  upload_file.save(os.path.join(app.config['UPLOAD_MATERIAL'], secure))
  # Calculate File Size #
  file_size = int(os.path.getsize('static/material/'+secure)/1024)

  material = Content(title=title, filename=secure, file_type=file_type, uploader=current_user.id, file_size=file_size)
  db.session.add(material)
  db.session.commit()

  new_material_id = Content.query.filter_by(filename=secure).first().id
  # Update Commission Content/Material List

  if commission_to_display.content == None:
      commission_to_display.content = ";" + str(new_material_id)
  else:
      commission_to_display.content = commission_to_display.content + ";" + str(new_material_id)
  db.session.commit()

  return redirect(url_for('manage_materials', id=Cid))


@app.route('/remove_material/<int:Mid> <int:Cid>', methods=['GET','POST'])
def remove_material(Mid, Cid):
    commission_to_update = Commissions.query.filter_by(id=Cid).first()
    str_to_remove = ";" + str(Mid)
    commission_to_update.content = commission_to_update.content.replace(str_to_remove, "", 1)
    db.session.commit()
    return redirect(url_for('manage_materials', id=Cid))

@app.route('/download_material/<int:Mid>', methods=['GET','POST'])
def download_material(Mid):
    material_to_download = Content.query.filter_by(id=Mid).first()
    path = "./" + app.config['UPLOAD_MATERIAL'] + "/" + material_to_download.filename

    return send_file(path,as_attachment=True)


@app.route('/add_storyboards/<int:id>', methods=['GET','POST'])
def manage_storyboards(id):
    commission_to_display = Commissions.query.filter_by(id=id).first()
    storyboards_to_display = []
    storyboard_materials_dict = {}
    add_frame_form = Add_Frame_Form()
    create_storyboards_form = Create_Storyboards_Form()
    upload_materials_form = Upload_Materials_Form()

    if request.method == "POST":
      title = request.form['title']
      status = request.form['status']
      # Create New Storyboard In Storyboards DB
      storyboard = Storyboards(title=title, status=status)
      db.session.add(storyboard)
      db.session.commit()

      new_storyboard_id = storyboard.id
      flash("New Storyboard Created")
      if commission_to_display.storyboards == None:
          commission_to_display.storyboards = ";" + str(new_storyboard_id)
      else:
          commission_to_display.storyboards = commission_to_display.storyboards + ";" + str(new_storyboard_id)
      db.session.commit()

    return redirect(url_for('manage_materials', id=id))

@app.route('/delete_storyboard/<int:Sid> <int:Cid>')
def delete_storyboard(Sid, Cid):
    commission_to_update = Commissions.query.filter_by(id=Cid).first()
    str_to_remove = ";" + str(Sid)
    commission_to_update.storyboards = commission_to_update.storyboards.replace(str_to_remove, "", 1)
    db.session.commit()
    return redirect(url_for('manage_materials', id=Cid))

@app.route('/add_storyboard_frame/<int:Sid> <int:Cid>', methods=['GET','POST'])
def add_storyboard_frame(Sid, Cid):

    storyboard_to_update = Storyboards.query.filter_by(id=Sid).first()

    if request.method == "POST":
        upload_file = request.files['frame']
        # Give PFP Unique Filename
        title = upload_file.filename
        secure = secure_filename(title)
        secure = str(uuid.uuid1()) + "_" + secure
        file_type = title.split('.')[1]
        # Save Image
        upload_file.save(os.path.join(app.config['UPLOAD_MATERIAL'], secure))
        # Calculate File Size #
        file_size = int(os.path.getsize('static/material/'+secure)/1024)

        material = Content(title=title, filename=secure, file_type=file_type, uploader=current_user.id, file_size=file_size)
        db.session.add(material)


        new_material_id = Content.query.filter_by(filename=secure).first().id

        if storyboard_to_update.content_list == None:
            storyboard_to_update.content_list = ";" + str(new_material_id)
        else:
            storyboard_to_update.content_list = storyboard_to_update.content_list + ";" + str(new_material_id)
        db.session.commit()

    return redirect(url_for('manage_materials', id=Cid))

@app.route('/settings', methods=['GET','POST'])
def settings():
    settings_form = Settings_Form()
    settings_delete = Settings_Delete()

    if settings_delete.delete.data and settings_delete.validate_on_submit():
        if settings_delete.delete_account.data == "DELETE":
            return redirect(url_for('confirm_delete'))
        else:
            flash("Please confirm your account deletion by typing 'DELETE' into the above text field.")
    choices = ['Active','Away','Do Not Disturb','Inactive']
    settings_form.status.choices = choices
    settings_form.status.default = current_user.status
    settings_form.colourblind.default = current_user.colour_scheme
    settings_form.process()

    return render_template('settings.html', current_page='settings', settings_form=settings_form,
    settings_delete=settings_delete, choices=choices)

@app.route('/confirm_delete', methods=['GET','POST'])
def confirm_delete():
    confirm_delete_form = Confirm_Delete_Form()
    if request.method == "POST":
        return redirect(url_for('delete_user', id=current_user.id))

    return render_template('confirm_delete.html', confirm_delete_form=confirm_delete_form)

@app.route('/update_settings', methods=['GET','POST'])
def update_settings():
    current_user.colour_scheme = request.form['colourblind']
    flash(request.form['status'])
    current_user.status = request.form['status']
    db.session.commit()

    return redirect(url_for('settings'))


# Error Pages
@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template("500.html"), 500
